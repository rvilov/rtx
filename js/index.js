$(function () {
    
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
   
        
    $('.carousel').carousel({
     // json para establecer las opciones
     interval: 3000
    });
     //el on siempre se usa para eventos 
    $('#staticBackdrop').on('show.bs.modal', function(e){
        
        console.log("Modal mostrandose");

        $('#pedidoBtn').removeClass("btn2");
        $('#pedidoBtn').addClass("btn-info");
        $('#pedidoBtn').prop('disabled', true);

    })

    $('#staticBackdrop').on('shown.bs.modal', function(e){
        console.log("Modal mostrado");
    })

    $('#staticBackdrop').on('hide.bs.modal', function(e){
        console.log("Modal cerrando");
    })


    $('#staticBackdrop').on('hidden.bs.modal', function(e){
        console.log("Modal cerrado")
        $('#pedidoBtn').prop('disabled',false);
        $('#pedidoBtn').removeClass("btn-info");
        $('#pedidoBtn').addClass("btn2");
    })
});